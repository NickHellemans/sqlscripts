USE aptunes;
DROP procedure IF EXISTS MockAlbumRelease;

DELIMITER $$
CREATE PROCEDURE MockAlbumRelease ()
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;

select count(*) into numberOfAlbums from Albums;
select count(*) into numberOfBands from Bands;
set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId = FLOOR(RAND() * numberOfBands) + 1;

  
IF  (randomBandId,randomAlbumId) not in (select * from Albumreleases) then
insert into AlbumReleases(Albums_Id, Bands_Id) values(randomAlbumId, randomBandId);
end if;

END$$

DELIMITER ;
