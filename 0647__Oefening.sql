USE aptunes;
DROP procedure IF EXISTS MockAlbumReleases;

DELIMITER $$
CREATE PROCEDURE MockAlbumReleasesLoop (in extraReleases int)
BEGIN
DECLARE counter INT DEFAULT 0;
declare success bool;

    loop_MockAlbumReleaseWithSuccess: loop
    call MockAlbumReleaseWithSuccess(success);
	if success = 1 then
    set counter = counter + 1;
    end if;
    if counter = extraReleases then
    leave loop_MockAlbumReleaseWithSuccess;
    end if;
    END loop;

END$$

DELIMITER ;
