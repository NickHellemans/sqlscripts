use aptunes;
DROP USER IF EXISTS student@localhost;
CREATE USER student@localhost
IDENTIFIED BY 'ikbeneenstudent';

GRANT EXECUTE ON procedure aptunes.GetAlbumDuration TO student@localhost;