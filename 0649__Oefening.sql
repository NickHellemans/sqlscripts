USE aptunes;
DROP procedure IF EXISTS DemonstrateHandlerOrder;

DELIMITER $$
CREATE PROCEDURE DemonstrateHandlerOrder ()
BEGIN
DECLARE randomNum INT DEFAULT 0;
DECLARE exit HANDLER FOR SQLSTATE '45002'
begin
        select  'State 45002 opgevangen. Geen probleem.!' Message;
end;
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
begin
RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
end;


set randomNum = FLOOR(RAND() * 3) + 1;

if(randomNum = 1) then
signal sqlstate '45001';

elseif (randomNum = 2) then
signal sqlstate '45002';

else 
signal sqlstate '45003';
end if;



  


END$$

DELIMITER ;