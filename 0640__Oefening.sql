USE aptunes;
DROP procedure IF EXISTS GetLiedjes;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE GetLiedjes(IN nameAlbum VARCHAR(50))
BEGIN
    SELECT Titel
    FROM Liedjes
    WHERE Titel LIKE CONCAT('%', nameAlbum, '%');
    
End$$

DELIMITER ;