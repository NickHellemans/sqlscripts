USE aptunes;
DROP procedure IF EXISTS DangerousInsertAlbumreleases;

DELIMITER $$
CREATE PROCEDURE DangerousInsertAlbumreleases ()
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomNum INT DEFAULT 0;

DECLARE exit HANDLER FOR SQLexception
begin
rollback;
select 'Error: Nieuwe releases konden niet worden toegevoegd.' Message;
end;
start transaction;
select count(*) from Albums into numberOfAlbums;
select count(*) from Bands into numberOfBands;
set randomNum = FLOOR(RAND() * 3) + 1;

insert into albumreleases (Albums_Id, Bands_id)
values
(FLOOR(RAND() * numberOfAlbums) + 1, FLOOR(RAND() * numberOfBands) + 1);

if (randomNum = 1)then
signal sqlstate '45000';
else
insert into albumreleases (Albums_Id, Bands_id)
values
(FLOOR(RAND() * numberOfAlbums) + 1, FLOOR(RAND() * numberOfBands) + 1);
end if;

insert into albumreleases (Albums_Id, Bands_id)
values
(FLOOR(RAND() * numberOfAlbums) + 1, FLOOR(RAND() * numberOfBands) + 1);

commit;
END$$

DELIMITER ;