-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
   
-- Overige kolommen aan de tabel Personen toevoegen + primary key
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
-- link tussen tabel personen en boeken voorzie adhv foreign key, om de error te omzeilen moeten we bij het toevoegen van de kolom de  not null constraint
-- laten vallen en later pas toevoegen
alter table Boeken add Personen_Id int null;

-- De waarde van Id van personen tabel kopieren naar de kolom Personen_Id van boeken
SET SQL_SAFE_UPDATES = 0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;

-- foreign key verplicht maken, dus toevoegen van not null
alter table Boeken change Personen_Id Personen_Id int not null;

-- Dubbele kolommen verwijderen uit de tabel Boeken
alter table Boeken drop column Voornaam,
    drop column Familienaam;
  
-- De foreign key constraint op de kolom Personen_Id toevoegen
-- dan de constraint toevoegen
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);
