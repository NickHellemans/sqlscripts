USE aptunes;
DROP procedure IF EXISTS NumberOfGenres;

DELIMITER $$
CREATE PROCEDURE NumberOfGenres (OUT total tinyint)
BEGIN
    SELECT COUNT(*)
    INTO total
    FROM Genres;
END$$

DELIMITER ;