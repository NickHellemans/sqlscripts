use ModernWays;
create table Uitleningen
(Startdatum date not null,
Einddatum date,
Boeken_Id int not null,
constraint fk_Uitleningen_Boeken foreign key (Boeken_Id)
references Boeken(Id),
Leden_Id int not null,
constraint fk_Uitleningen_Leden foreign key (Leden_Id)
references Leden(Id))