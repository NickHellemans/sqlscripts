use ModernWays;
create table Student (
Voornaam varchar (100) char set utf8mb4 not null,
Familienaam varchar (100) char set utf8mb4 not null,
Studentennummer int auto_increment PRIMARY KEY);

create table Opleiding (
Naam varchar(100) char set utf8mb4 PRIMARY KEY);

create table Vak (
Naam varchar(100) char set utf8mb4 primary key);

create table Lector (
Voornaam varchar (100) char set utf8mb4 not null,
Familienaam varchar (100) char set utf8mb4 not null,
Personeelsnummer int auto_increment PRIMARY KEY);

alter table Student add column Opleiding_Naam varchar(100) char set utf8mb4 not null,
add column Semester tinyint unsigned not null,
add constraint fk_Student_Opleiding 
foreign key (Opleiding_Naam)
references Opleiding(Naam);

create table LectorGeeftVak (
Lector_Personeelsnummer int not null,
Vak_Naam  varchar (100) char set utf8mb4,
constraint fk_LectorGeeftVak_Lector
foreign key (Lector_Personeelsnummer)
references Lectoren(Personeelsnummer),
constraint fk_LectorGeeftVak_Vak
foreign key (Vak_Naam)
references Vak(Naam));

create table 