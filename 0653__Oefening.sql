USE aptunes;
DROP procedure IF EXISTS GetAlbumDuration2;

DELIMITER $$
CREATE DEFINER=root@localhost PROCEDURE GetAlbumDuration2 (in albumId int, out totalDuration SMALLINT UNSIGNED )
SQL SECURITY invoker

BEGIN
DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
DECLARE ok bool DEFAULT false;
  DECLARE currentSong
  CURSOR FOR select Lengte from Liedjes where Albums_Id = albumId;
  
  DECLARE CONTINUE HANDLER
  FOR NOT FOUND SET ok = true;
  
  set totalDuration = 0;
  open currentSong;
    getSong: LOOP
        FETCH currentSong INTO songDuration;
    IF ok = true
    THEN
            LEAVE getSong;
        END IF;
SET totalDuration = totalDuration + songDuration;
END LOOP getSong;

  CLOSE currentSong;



END$$

DELIMITER ;