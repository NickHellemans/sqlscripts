use ModernWays;
select Leeftijdscategorie, avg(Personeelsleden.Loon) as 'Gemiddeld loon' from 
(select floor(Leeftijd/10)*10 as Leeftijdscategorie, Loon from Personeelsleden) as Leeftijdscategorieen
inner join Personeelsleden on Personeelsleden.Loon = Leeftijdscategorieen.Loon
group by Leeftijdscategorie

