USE aptunes;
DROP procedure IF EXISTS MockAlbumReleaseWithSuccess;

DELIMITER $$

CREATE PROCEDURE MockAlbumReleaseWithSuccess (out succes bool)
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;

select count(*) into numberOfAlbums from Albums;
select count(*) into numberOfBands from Bands;
set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId = FLOOR(RAND() * numberOfBands) + 1;

  
IF  (randomBandId,randomAlbumId) not in (select * from Albumreleases) then
insert into AlbumReleases(Albums_Id, Bands_Id) values(randomAlbumId, randomBandId);
set succes = 1;
else
set succes = 0;
end if;

END$$

DELIMITER ;
