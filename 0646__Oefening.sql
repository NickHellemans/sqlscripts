USE aptunes;
DROP procedure IF EXISTS MockAlbumReleases;

DELIMITER $$
CREATE PROCEDURE MockAlbumReleases (in extraReleases int)
BEGIN
DECLARE counter INT DEFAULT 0;
declare success bool;
    REPEAT
	call MockAlbumReleaseWithSuccess(success);
    if success = 1 then
	SET counter = counter + 1;
    end if;
    UNTIL counter = extraReleases
    END REPEAT;

END$$

DELIMITER ;
