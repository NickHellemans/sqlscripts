USE aptunes;
DROP procedure IF EXISTS CreateAndReleaseAlbum;

DELIMITER $$
CREATE PROCEDURE CreateAndReleaseAlbum (IN titelAlbum varchar(100), in band_Id INT)
BEGIN
start transaction;
  insert into Albums (Titel)
  values (titelAlbum);
  insert into Albumreleases (Bands_Id, Albums_Id)
  values (band_Id, last_insert_id());
commit;
END$$

DELIMITER ;