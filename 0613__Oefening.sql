use ModernWays;
create view AuteursBoeken
as 
select Boeken.Titel, concat(Personen.Voornaam, ' ', Personen.Familienaam) as Auteur from Publicaties
inner join Boeken on Boeken_Id = Boeken.Id
inner join Personen on Personen_Id = Personen.Id
