USE aptunes;
DROP procedure IF EXISTS CleanupOldMemberships;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE CleanupOldMemberships(IN someDate date, out numberCleaned INT)
BEGIN
start transaction;
select count(*) into numberCleaned from Lidmaatschappen where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < someDate;

    SET SQL_SAFE_UPDATES = 0;
delete from lidmaatschappen where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < someDate;
    SET SQL_SAFE_UPDATES = 1;

commit;
END$$
DELIMITER ;