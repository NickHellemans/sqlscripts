USE aptunes;
DROP procedure IF EXISTS GetAlbumDuration;

DELIMITER $$
CREATE PROCEDURE GetAlbumDuration (in albumId int, out totalDuration SMALLINT UNSIGNED )
BEGIN
DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
DECLARE ok bool DEFAULT false;
  DECLARE currentSong
  CURSOR FOR select Lengte from Liedjes where Albums_Id = albumId;
  
  DECLARE CONTINUE HANDLER
  FOR NOT FOUND SET ok = True;
  
  set totalDuration = 0;
  open currentSong;
    getSong: LOOP
        FETCH currentSong INTO songDuration;
    IF ok = true
    THEN
            LEAVE getSong;
        END IF;
SET totalDuration = totalDuration + songDuration;
END LOOP getSong;

  CLOSE currentSong;



END$$

DELIMITER ;